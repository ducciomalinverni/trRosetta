# *trRosetta*
This repository is a fork of the original ***trRosetta***(https://github.com/gjoni/trRosetta) package by Ivan Anishchenko et al., and implements some utility functions and the full folding pipeline based on pyrosetta.

## Note
Please source the included virtual environment to use the correct librairies, i.e. source envOSX/bin/activate.
The virtualenv contains the following package versions:
* tensorflow-1.14.0
* pyrosetta-3

Change the base paths in predictGeoemtries and fold to the path containing this repository.

## ***Usage***: Predicting the geometries (without explicit folding)
Generates the set of gemetries computed from the family MSA.
Note that msa.fasta must have each sequence written to a single line (no multi-line fasta).

```
./predictGeometries msa.fasta out.npz
```

out.npz is a numpy compressed file containing the following structures:
* 'dist' : Distograms, NxNx37 array
* 'omega': Torsion angles, NxNx25 array
* 'theta': Torsion angles, NxNx25 array
* 'phi': Torsion angles, NxNx13 array

## ***Usage***: Folding the sequence
Fold multiple structural models of the sequence in seq.fasta, from the geometries computed by the NN stage in in.npz.

./fold seq.fasta in.npz nModels pdbPrefix


## References
[J Yang, I Anishchenko, H Park, Z Peng, S Ovchinnikov, D Baker. Improved protein structure prediction using predicted inter-residue orientations. (2020) PNAS. 117(3): 1496-1503](https://www.pnas.org/content/117/3/1496)
